﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Game.Board.Detail
{
    public class GameUtilityEditor : MonoBehaviour
    {
        [MenuItem("GameUtility/CreateNewManager")]
        public static void CreateBoardManager()
        {
            BoardManager currentManager = GameObject.FindObjectOfType<BoardManager>();
            if (currentManager == null)
            {
                GameObject newBoardManager = new GameObject();
                newBoardManager.transform.position = Vector3.zero;
                newBoardManager.name = "Board Manager";
                newBoardManager.AddComponent<BoardManager>();
                //newBoardManager.AddComponent<BoardRowGenerator>();
            }
        }
    }
}