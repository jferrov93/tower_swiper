﻿using Game.Board.Detail;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using Util;

namespace Game.Board.Detail
{
    [CustomEditor(typeof(BoardElement), true)]
    public class BoardElementEditor : Editor
    {
        private BoardManager boardManager;

        public void OnEnable()
        {
            boardManager = FindObjectOfType<BoardManager>();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            
            UpdatePosition();
            UpdateTile();
        }

        private void UpdateTile()
        {
            if (boardManager == null) { return; }
            BoardElement element = target as BoardElement;
            if (PrefabUtility.GetPrefabParent(element.gameObject) == null) { return; }
            TileBase tile = boardManager.GetTile(element.Coordinates) as TileBase;
            element.transform.parent = tile.transform;
        }

        private void UpdatePosition()
        {
            NormalizeCoords();
            if (boardManager == null) { return; }
            BoardElement element = target as BoardElement;
            element.transform.position = boardManager.GetTile(element.Coordinates).WorldPos;
        }

        private void NormalizeCoords()
        {
            if (boardManager == null) { return; }
            BoardElement element = target as BoardElement;
            Int2 coords = element.Coordinates;
            coords = Int2.SquareClamp(coords, Int2.zero, new Int2(boardManager.board.RowCount -1, boardManager.board[0].Length-1));
            element.SetCoordinates(coords);
        }
    }
}