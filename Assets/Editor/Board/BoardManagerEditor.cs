﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace Game.Board.Detail
{
    [CustomEditor(typeof(BoardManager))]
    public class BoardManagerEditor : Editor
    {
        private bool showNewBoard = false;

        private string dataPath = "Assets/Resources/TilePrefabsData.asset";
        private TilesData data;
        private int rows, columns;
        private float tileSize = 1;

        private void OnEnable()
        {
            data = AssetDatabase.LoadAssetAtPath(dataPath, typeof(TilesData)) as TilesData;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("New Board"))
            {
                showNewBoard = !showNewBoard;
            }

            if (showNewBoard)
            {
                NewBoardOptions();
            }
        }

        private void NewBoardOptions()
        {
            data = (TilesData) EditorGUILayout.ObjectField("Tiles Data", data, typeof(TileBase), true);
            rows = EditorGUILayout.IntField("Rows", rows);
            columns = EditorGUILayout.IntField("Columns", columns);
            tileSize = EditorGUILayout.FloatField("Tile Size", tileSize);

            if (GUILayout.Button("Build Board"))
            {
                BuildBoard();
            }
        }

        private void BuildBoard()
        {
            if (data == null || rows == 0 || columns == 0) { return; }
            BoardManager manager = target as BoardManager;

            Board newBoard = new Board(rows, columns);
            for (int i = 0; i < newBoard.RowCount; i++)
            {
                for (int j = 0; j < newBoard[i].Length; j++)
                {
                    newBoard[i][j] = PrefabUtility.InstantiatePrefab(data.BasicTile) as TileBase;
                    newBoard[i][j].transform.parent = manager.transform;
                    newBoard[i][j].transform.localPosition = new Vector2(j * tileSize, i * tileSize);
                }
            }

            manager.board = newBoard;
        }
    }
}