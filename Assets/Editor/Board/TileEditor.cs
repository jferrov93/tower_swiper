﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Game.Board.Detail
{
    [CustomEditor(typeof(TileBase), true)]
    public class TileEditor : Editor
    {
        private string dataPath = "Assets/Resources/TilePrefabsData.asset";
        private TilesData data;

        private bool changingTile = false;

        private void OnEnable()
        {
            data = AssetDatabase.LoadAssetAtPath(dataPath, typeof(TilesData)) as TilesData;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Change Tile Type"))
            {
                changingTile = !changingTile;
            }

            if (changingTile)
            {
                ChangeTileOptions();
            }
        }

        private void ChangeTileOptions()
        {

            for (int i = 0; i < data.Tiles.Length; i++)
            {
                if (GUILayout.Button(data.Tiles[i].GetType().ToString().Split('.').Last()))
                {
                    ChangeTile(data.Tiles[i]);
                }
            }
            
        }

        private void ChangeTile(TileBase newTile)
        {
            Debug.Log(newTile.GetType().ToString().Split('.').Last());
            BoardManager manager = (target as TileBase).transform.parent.GetComponent<BoardManager>();
            for (int i = 0; i < manager.board.RowCount; i++)
            {
                for (int j = 0; j < manager.board[i].Length; j++)
                {
                    if (manager.board[i][j] == (target as TileBase))
                    {
                        TileBase oldTile = manager.board[i][j];
                        manager.board[i][j] = PrefabUtility.InstantiatePrefab(newTile) as TileBase;
                        manager.board[i][j].transform.localPosition = oldTile.transform.localPosition;
                        manager.board[i][j].transform.parent = oldTile.transform.parent;
                        manager.board[i][j].transform.SetSiblingIndex(oldTile.transform.GetSiblingIndex());
                        DestroyImmediate(oldTile.gameObject);
                    }
                }
            }
        }
    }
}