﻿using Game.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.Ingame.Detail
{
    public class SpecialTexts : MonoBehaviour
    {
        [Inject]
        private IPlayer player;

        private Text text;

        private void Awake()
        {
            text = GetComponent<Text>();
        }

        private void Update()
        {
            string specialText = "";
            for (int i = 0; i < player.PlayerWallet.CurrentSpecials.Length; i++)
            {
                specialText += player.PlayerWallet.CurrentSpecials[i].ToString();
                specialText += ", ";
            }
            text.text = specialText;
        }
    }
}