﻿using Game.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace UI.Ingame.Detail
{
    [RequireComponent(typeof(Text))]
    public class CoinText : MonoBehaviour
    {
        [Inject]
        private IPlayer player;

        private Text text;

        private void Awake()
        {
            text = GetComponent<Text>();
        }

        private void Update()
        {
            text.text = player.PlayerWallet.Coins.ToString();
        }
    }
}