﻿using Game.Board;
using Game.Interactive;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Characters.Detail
{
    public class SkillInteract : SkillBase
    {
        private bool CanActivate
        {
            get { return !player.PlayerMovement.IsMoving; }
        }

        protected override void OnDirectionInput(Direction direction)
        {
            base.OnDirectionInput(direction);
            if (CanActivate && direction == Direction.up)
            {
                InteractWithTile();
            }
        }

        protected override void OnActionInput()
        {
            base.OnActionInput();
            InteractWithObject();
        }

        protected override void UpdateSkill()
        {
            
        }

        private void InteractWithObject()
        {
            List<BoardElement> tileElements = player.PlayerMovement.CurrentTile.TileElements;
            for (int i = 0; i < tileElements.Count; i++)
            {
                if (tileElements[i] is IIneractable)
                {
                    (tileElements[i] as IIneractable).Interact(player);
                }
            }
        }

        private void InteractWithTile()
        {
            IIneractable interactable = player.PlayerMovement.CurrentTile as IIneractable;
            if (interactable != null)
            {
                interactable.Interact(player);
            }
        }
    }
}