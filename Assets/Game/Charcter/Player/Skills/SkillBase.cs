﻿using GameInput;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Characters.Detail
{
    public abstract class SkillBase : MonoBehaviour
    {
        [Inject]
        protected Player player;
        [Inject]
        protected IInputManager input;

        private void Start()
        {
            input.Controller.DirectionInputUsed += OnDirectionInput;
            input.Controller.ActionInputUsed += OnActionInput;
        }

        private void Update()
        {
            if(!player.Frozen)
                UpdateSkill();
        }

        protected abstract void UpdateSkill();

        protected virtual void OnDirectionInput(Direction direction)
        { }

        protected virtual void OnActionInput()
        { }
    }
}