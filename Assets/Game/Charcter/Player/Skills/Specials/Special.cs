﻿using Game.Board;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Characters.Specials
{
    public abstract class Special : MonoBehaviour
    {
        [Inject]
        protected IBoardManager boardManager;
        [Inject]
        protected IPlayer player;

        public abstract void ExcecuteSpecial();
    }
}