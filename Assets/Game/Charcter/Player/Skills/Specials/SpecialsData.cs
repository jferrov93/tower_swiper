﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Characters.Specials
{
    [CreateAssetMenu(fileName = "SpecialsData", menuName = "Data/Specials")]
    public class SpecialsData : ScriptableObject
    {
        [SerializeField]
        private List<SpecialInfo> specials;

        public Special this[SpecialType specialType]
        {
            get { return specials.Find(info => info.SType == SpecialType.rope).TSpecial; }
        }
    }

    public enum SpecialType
    {
        rope
    }

    [Serializable]
    internal class SpecialInfo
    {
        [SerializeField]
        private SpecialType type;
        [SerializeField]
        private Special special;

        public SpecialType SType
        {
            get { return type; }
        }

        public Special TSpecial
        {
            get { return special; }
        }
    }
}