﻿using Game.Board;
using Game.Interactive;
using Game.Interactive.Detail;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Characters.Specials.Detail
{
    public class SpecialRope : Special
    {
        [SerializeField]
        private InteractableObject ladderUp, ladderDown;

        public override void ExcecuteSpecial()
        {
            if (player.PlayerMovement.Coords.x == 0) { Destroy(gameObject); return; }
            ITile tile = boardManager.GetTile(player.PlayerMovement.Coords);
            tile.AddElement(ladderDown);
            transform.position = boardManager.GetTile(player.PlayerMovement.Coords).WorldPos + Vector3.down * 0.5f;
            player.PlayerMovement.DOMovement(Direction.down);
            tile = boardManager.GetTile(player.PlayerMovement.Coords);
            tile.AddElement(ladderUp);
        }
    }
}