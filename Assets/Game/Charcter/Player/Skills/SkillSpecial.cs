﻿using Game.Characters.Specials;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Characters.Detail
{
    public class SkillSpecial : SkillBase
    {
        [Inject]
        private DiContainer container;

        protected override void UpdateSkill()
        {
            
        }

        protected override void OnDirectionInput(Direction direction)
        {
            base.OnDirectionInput(direction);
            if (direction == Direction.down)
            {
                Special prefab = player.PlayerWallet.PopSpecialPrefab();
                if (prefab == null) { return; }
                Special special = container.InstantiatePrefab(prefab).GetComponent<Special>();
                special.ExcecuteSpecial();
            }
        }
    }
}