﻿using DG.Tweening;
using Game.Board;
using GameInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;
using Zenject;

namespace Game.Characters.Detail
{
    public class PlayerMovement : Navigator, IPlayerMovement
    {
        [SerializeField]
        private Ease movementEase;

        [Inject]
        private Player player;
        [Inject]
        private IInputManager input;

        public bool IsMoving
        {
            get
            {
                return moving;
            }
        }

        private void Start()
        {
            input.Controller.DirectionInputUsed += OnDirectionInput;
        }

        protected override void OnMovementFinished()
        {
            base.OnMovementFinished();
            CurrentTile.OnPlayerArrival(player);
        }

        private void OnDirectionInput(Direction direction)
        {
            if (player.Frozen) { return; }
            if (direction == Direction.left || direction == Direction.right)
                DOMovement(direction, movementEase);
        }
    }
}

namespace Game.Characters
{
    public interface IPlayerMovement : INavigator
    {
        bool IsMoving { get; }
    }
}