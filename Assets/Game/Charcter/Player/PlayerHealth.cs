﻿using Game.Damage;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Characters.Detail
{
    public class PlayerHealth : MonoBehaviour, IDamageable
    {
        [Inject]
        private Player player;
        [Inject]
        private IGameManager gameManager;

        public void Damage()
        {
            Die();
        }

        private void Die()
        {
            player.Frozen = true;
            gameManager.Restart();
        }
    }
}