﻿using System;
using System.Collections;
using UnityEngine;
using Game.Characters.Specials;
using System.Collections.Generic;

namespace Game.Characters
{
    [Serializable]
    public class Wallet
    {
        private int coins = 0;

        [SerializeField]
        private List<SpecialType> specials = new List<SpecialType>(new SpecialType[] { SpecialType.rope });
        [SerializeField]
        private SpecialsData specialsData;

        public int Coins
        {
            get { return coins; }
        }

        public SpecialType[] CurrentSpecials
        {
            get { return specials.ToArray(); }
        }

        public Wallet()
        {
        }

        public void AddCoins(int coinsToAdd)
        {
            coins += coinsToAdd;
        }

        public void SubstractCoins(int coinsToSubstract)
        {
            coins -= coinsToSubstract;
        }

        public Special PopSpecialPrefab()
        {
            if (specials.Count == 0) { return null; }
            Special special = specialsData[specials[specials.Count - 1]];
            specials.RemoveAt(specials.Count-1);
            return special;
        }

        public void PushSpecial(SpecialType special)
        {
            specials.Add(special);
        }

        public void SetSpecials(SpecialType[] specials)
        {
            this.specials = new List<SpecialType>();
            for (int i = 0; i < specials.Length; i++)
            {
                this.specials.Add(specials[i]);
            }
        }
    }
}