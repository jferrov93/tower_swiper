﻿using Game.Characters.Specials;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Characters.Detail
{
    public class PlayerInstaller : MonoInstaller
    {
        [SerializeField]
        private Player player;
        [SerializeField]
        private SpecialsData specialData;

        public override void InstallBindings()
        {
            Container.Bind<Player>().FromInstance(player);
            Container.Bind<IPlayer>().FromInstance(player);

            Container.Bind<SpecialsData>().FromInstance(specialData);
        }
    }
}