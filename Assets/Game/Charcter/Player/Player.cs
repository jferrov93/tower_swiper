﻿using Game.Board;
using UnityEngine;
using Util;
using GameInput;
using Zenject;
using System;

namespace Game.Characters.Detail
{
    public class Player : MonoBehaviour, IPlayer
    {
        public event Action<Int2> PlayerMoved;

        public bool Frozen
        { get; set; }

        [Inject]
        private IInputManager input;
        [Inject]
        private IBoardManager boardManager;

        [SerializeField]
        private Wallet wallet;
        private PlayerMovement playerMovement;

        public IPlayerMovement PlayerMovement
        {
            get { return playerMovement; }
        }

        public Wallet PlayerWallet
        {
            get
            {
                return wallet;
            }
        }

        private void Awake()
        {
            playerMovement = GetComponent<PlayerMovement>();
            playerMovement.NavigatorArrived += RaisePlayerMoved;
        }

        private void RaisePlayerMoved(Int2 playerCoords)
        {
            if (PlayerMoved != null)
            {
                PlayerMoved(playerCoords);
            }
        }
    }
}

namespace Game.Characters
{
    public interface IPlayer
    {
        IPlayerMovement PlayerMovement { get; }

        Wallet PlayerWallet { get; }

        event Action<Int2> PlayerMoved;
    }
}
