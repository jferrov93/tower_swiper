﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Damage
{
    public interface IDamageable 
    {
        void Damage();
    }
}