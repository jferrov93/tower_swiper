﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Damage.Detail
{
    public class DamageOnContact : MonoBehaviour
    {
        private void OnTriggerStay2D(Collider2D collision)
        {
            IDamageable damageable = collision.GetComponent<IDamageable>();
            if (damageable != null)
            {
                damageable.Damage();
            }
        }
    }
}