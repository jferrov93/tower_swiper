﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Detail
{
    public class GameManagerInstaller : MonoInstaller
    {
        [SerializeField]
        private GameManager gameManager;

        public override void InstallBindings()
        {
            Container.Bind<IGameManager>().FromInstance(gameManager);
            Container.Bind<GameManager>().FromInstance(gameManager);
        }
    }
}