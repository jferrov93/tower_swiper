﻿using Game.Characters;
using Game.Damage.Detail;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Interactive.Detail
{
    internal class Spikes : Activable
    {
        public event Action<int> ChangedActivationStep;

        private int step = 0;
        private Coroutine activationRoutine;

        public float preActivationDelay = 0.8f;

        private Collider2D collider;
        private SpriteRenderer sprite;

        public int CurrentActivationStep
        {
            get { return step; }
        }

        private void Awake()
        {
            collider = GetComponent<Collider2D>();
            sprite = GetComponent<SpriteRenderer>();
        }

        private void Start()
        {
            ToggleSpike(IsActive);
        }

        protected override void OnActivate(bool activateOn)
        {
            if (activateOn && step == 0 && activationRoutine == null)
            {
                activationRoutine = StartCoroutine(ActivationRoutine());
            }
            else
            {
                if (activationRoutine != null) { StopCoroutine(activationRoutine); }
                activationRoutine = null;
                step = 0;
                RaiseChangedActivationStep();
                ToggleSpike(false);
            }
        }

        private IEnumerator ActivationRoutine()
        {
            ActivationStep1();
            yield return new WaitForSeconds(preActivationDelay);
            ActivationStep2();
        }

        private void ActivationStep1()
        {
            step = 1;
            RaiseChangedActivationStep();
        }

        private void ActivationStep2()
        {
            step = 2;
            RaiseChangedActivationStep();
            ToggleSpike(true);
        }

        private void RaiseChangedActivationStep()
        {
            if (ChangedActivationStep != null)
            {
                ChangedActivationStep(CurrentActivationStep);
            }
        }

        private void ToggleSpike(bool on)
        {
            collider.enabled = on;
            sprite.enabled = on;
        }
    }
}