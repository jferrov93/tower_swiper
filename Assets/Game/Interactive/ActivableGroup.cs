﻿using System;
using System.Collections;
using UnityEngine;

namespace Game.Interactive.Detail
{
    public class ActivableGroup : MonoBehaviour
    {
        [SerializeField]
        private bool looped;
        [SerializeField]
        private ActivablesRoutine[] routines;

        private int index = 0;

        private void Start()
        {
            ExcecuteCurrentRoutine();
        }

        private void ExcecuteCurrentRoutine()
        {
            routines[index].StartSequence(this, nextStep);
        }

        private void nextStep()
        {
            index++;
            if (index < routines.Length)
            {
                ExcecuteCurrentRoutine();
            }
            else if(looped)
            {
                index = 0;
                ExcecuteCurrentRoutine();
            }
        }

    }

    [Serializable]
    internal class ActivablesRoutine
    {
        [SerializeField]
        private Activable[] activables;
        [SerializeField]
        private float delayedActivation = 0f;
        [SerializeField]
        private float stepDuration = 1f;
        [SerializeField]
        private ActivationMode[] steps = { ActivationMode.toggle };

        private int index;
        private bool active;

        private MonoBehaviour routineExcecuter;
        private Action onComplete;

        private enum ActivationMode
        {
            allOn, allOff, toggle
        }

        public void StartSequence(MonoBehaviour routineExcecuter, Action onComplete)
        {
            index = 0;
            this.routineExcecuter = routineExcecuter;
            this.onComplete = onComplete;
            this.routineExcecuter.StartCoroutine(ActivationRoutine());
        }

        private IEnumerator ActivationRoutine()
        {
            if (index == 0)
            {
                yield return new WaitForSeconds(delayedActivation);
            }

            while (index < steps.Length)
            {
                ExcecuteActivation(steps[index]);

                yield return new WaitForSeconds(stepDuration);
                index++;
            }
            EndSequence();
        }

        private void ExcecuteActivation(ActivationMode mode)
        {
            switch (mode)
            {
                case ActivationMode.allOff:
                case ActivationMode.allOn:
                    foreach (Activable activable in activables)
                    {
                        activable.Activate(mode == ActivationMode.allOn);
                    }
                    break;
                case ActivationMode.toggle:
                    foreach (Activable activable in activables)
                    {
                        activable.ToggleActive();
                    }
                    break;
            }
        }

        private void EndSequence()
        {
            onComplete();
        }
    }
}