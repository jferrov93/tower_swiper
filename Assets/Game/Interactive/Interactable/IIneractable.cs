﻿using Game.Characters;

namespace Game.Interactive
{
    internal interface IIneractable
    {
        void Interact(IPlayer player);
    }
}