﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Board;
using Game.Characters;
using UnityEngine;
using Util;
using Zenject;

namespace Game.Interactive.Detail
{
    public abstract class InteractableObject : BoardElement, IIneractable
    {
        public abstract void Interact(IPlayer player);
    }
}