﻿using System.Collections;
using System.Collections.Generic;
using Game.Board;
using Game.Characters;
using UnityEngine;
using Zenject;

namespace Game.Interactive.Detail
{
    public class Chest : InteractableObject
    {
        [SerializeField]
        private BoardElement contentPrefab;

        [Inject]
        private DiContainer container;

        public override void Interact(IPlayer player)
        {
            if (contentPrefab != null)
            {
                BoardElement element = container.InstantiatePrefab(contentPrefab).GetComponent<BoardElement>();
                boardManager.GetTile(Coordinates).AddElement(element);
            }

            Destroy(gameObject);
        }


    }
}