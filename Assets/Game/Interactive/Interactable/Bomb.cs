﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Characters;
using UnityEngine;

namespace Game.Interactive.Detail
{
    public class Bomb : InteractableObject
    {
        [SerializeField]
        private GameObject explosionCollider;
        [SerializeField]
        private float bombTimer = 1f;
        private float explosionDuration = 0.4f;

        private void Awake()
        {
            explosionCollider.SetActive(false);
            StartCoroutine(DelayedExplosion());
        }

        private IEnumerator DelayedExplosion()
        {
            yield return new WaitForSeconds(bombTimer);
            StartCoroutine(Explode());
        }

        public override void Interact(IPlayer player)
        {
            StartCoroutine(Explode());
        }

        private IEnumerator Explode()
        {
            explosionCollider.SetActive(true);
            yield return new WaitForSeconds(explosionDuration);
            Destroy(gameObject);
        }
    }
}