﻿using System.Collections;
using System.Collections.Generic;
using Game.Characters;
using UnityEngine;

namespace Game.Interactive.Detail
{
    public class Ladder : InteractableObject
    {
        public bool goingUp;

        public override void Interact(IPlayer player)
        {
            if (goingUp)
                player.PlayerMovement.DOMovement(Direction.up);
            else
                player.PlayerMovement.DOMovement(Direction.down);
        }
    }
}