﻿using System.Collections;
using System.Collections.Generic;
using Game.Characters;
using UnityEngine;

namespace Game.Interactive.Detail
{
    public class Coin : InteractableObject
    {
        [SerializeField]
        private int value = 2;

        public override void Interact(IPlayer player)
        {
            player.PlayerWallet.AddCoins(value);
            Destroy(gameObject);
        }
    }
}