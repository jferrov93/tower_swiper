﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Interactive
{
    public abstract class Activable : MonoBehaviour
    {
        public bool IsActive { get; private set; }

        public void Activate(bool activateOn)
        {
            IsActive = activateOn;
            OnActivate(IsActive);
        }

        public void ToggleActive()
        {
            Activate(!IsActive);
        }

        protected abstract void OnActivate(bool active);
    }
}