﻿using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInput.Detail
{
    [Serializable]
    public abstract class ControlBase: IControl
    {
        public event Action ActionInputUsed;
        public event Action<Direction> DirectionInputUsed;

        protected void RaiseDirectionInputUsed(Direction direction)
        {
            if (DirectionInputUsed != null)
                DirectionInputUsed(direction);
        }

        protected void RaiseActionInputUsed()
        {
            if (ActionInputUsed != null)
            {
                ActionInputUsed();
            }
        }

        public abstract void Update();
    }
}

namespace GameInput
{
    public interface IControl
    {
        event Action<Direction> DirectionInputUsed;
        event Action ActionInputUsed;
    }
}