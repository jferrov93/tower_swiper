﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInput.Detail
{
    public class InputManager : MonoBehaviour, IInputManager
    {
        [SerializeField]
        private SwipeControl swipeControl;

        private KeyboardControl keyboard;

        public IControl Controller
        {
            get { return swipeControl; }
        }

        private void Awake()
        {
            keyboard = new KeyboardControl();
        }

        private void Update()
        {
            swipeControl.Update();
        }
    }
}

namespace GameInput
{
    public interface IInputManager
    {
        IControl Controller { get; }
    }
}