﻿using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

namespace GameInput.Detail
{
    public class KeyboardControl : ControlBase
    {
        public bool WasNorthPressed
        {
            get
            {
                return Input.GetKeyDown(KeyCode.UpArrow);
            }
        }

        public bool WasEastPressed
        {
            get
            {
                return Input.GetKeyDown(KeyCode.RightArrow);
            }
        }

        public bool WasSouthPressed
        {
            get
            {
                return Input.GetKeyDown(KeyCode.DownArrow);
            }
        }

        public bool WasWestPressed
        {
            get
            {
                return Input.GetKeyDown(KeyCode.LeftArrow);
            }
        }

        public Direction DirectionPressed
        {
            get
            {
                if (WasNorthPressed) { return Direction.up; }
                if (WasEastPressed) { return Direction.right; }
                if (WasSouthPressed) { return Direction.down; }
                if (WasWestPressed) { return Direction.left; }
                return Direction.none;
            }
        }

        public override void Update()
        {
            if (DirectionPressed != Direction.none)
            {
                RaiseDirectionInputUsed(DirectionPressed);
            }
        }
    }
}

