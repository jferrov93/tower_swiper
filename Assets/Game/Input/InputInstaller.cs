﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace GameInput.Detail
{
    public class InputInstaller : MonoInstaller
    {
        [SerializeField]
        private InputManager inputManager;

        public override void InstallBindings()
        {
            Container.Bind<IInputManager>().FromInstance(inputManager);
        }
    }
}