﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;

namespace GameInput.Detail
{
    [Serializable]
    public class SwipeControl : ControlBase
    {
        public event Action TouchStarted;
        public event Action TouchEnded;
        public event Action<Vector3, Vector3> TwofingersTouch;

        [SerializeField]
        private TouchInputManager touchInput;
        [SerializeField]
        protected float maxSwipeTime = 1f;
        [SerializeField, Range(1, 100)]
        private float minSwipeMagnitude = 10f;

        bool alreadySwiped = false;

        protected ITouchInfo PrimaryTouch
        {
            get { return touchInput.InputDevice.PrimaryTouch; }
        }

        protected ITouchInfo SecondaryTouch
        {
            get { return touchInput.InputDevice.SecondaryTouch; }
        }

        protected float MinDisplacement
        {
            get { return (float)Screen.height * (minSwipeMagnitude / 100); }
        }

        public override void Update()
        {
            if (PrimaryTouch.StartedTouchThisFrame)
                RaiseTouchStarted();

            if (PrimaryTouch.IsTouching)
                CheckSwipe();

            if (PrimaryTouch.ReleasedTouchThisFrame)
            {
                alreadySwiped = false;
            }

            if (PrimaryTouch.IsTouching && SecondaryTouch.IsTouching)
                RaiseTwoFingersTouch();

            if (PrimaryTouch.ReleasedTapThisFrame && PrimaryTouch.TapCount == 1)
            {
                RaiseActionInputUsed();
            }
        }

        protected virtual void CheckSwipe()
        {
            RaiseTouchEnded();
            bool displacement = PrimaryTouch.DragVector.magnitude > MinDisplacement;
            if (displacement && !alreadySwiped)
            {
                SelectDirection(PrimaryTouch.DragVector);
                alreadySwiped = true;
            }
        }

        private void SelectDirection(Vector3 dragVector)
        {
            Direction direction = Direction.none;
            if (Mathf.Abs(dragVector.x) > Mathf.Abs(dragVector.y))
            {
                direction = dragVector.x >= 0 ? Direction.right : Direction.left;
            }
            else
            {
                direction = dragVector.y >= 0 ? Direction.up : Direction.down;
            }
            RaiseDirectionInputUsed(direction);
        }

        protected void RaiseTouchStarted()
        {
            if (TouchStarted != null)
                TouchStarted();
        }

        protected void RaiseTouchEnded()
        {
            if (TouchEnded != null)
                TouchEnded();
        }

        protected void RaiseTwoFingersTouch()
        {
            if (TwofingersTouch != null)
                TwofingersTouch(PrimaryTouch.TouchPosition, SecondaryTouch.TouchPosition);
        }
    }
}