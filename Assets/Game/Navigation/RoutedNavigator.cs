﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

namespace Game.Characters.Detail
{
    public class RoutedNavigator : Navigator
    {
        [SerializeField]
        private Direction[] steps = { Direction.right };
        [SerializeField]
        private Ease movementEase = Ease.InOutCubic;
        [SerializeField]
        private float waitInterval = 1f;
        [SerializeField]
        private bool looped = true;

        private int stepIndex = 0;
        private bool returning = false;

        private void Start()
        {
            StartCoroutine(WaitStep());
        }

        protected override void OnMovementFinished()
        {
            base.OnMovementFinished();
            StartCoroutine(WaitStep());
        }

        private IEnumerator WaitStep()
        {
            yield return new WaitForSeconds(waitInterval);
            DONextStep();
        }

        private void DONextStep()
        {
            if (stepIndex == steps.Length)
            {
                if (!looped) { return; }
                stepIndex = 0;
                DONextStep();
                return;
            }
            DOMovement(steps[stepIndex], movementEase);
            stepIndex++;
        }
    }
}