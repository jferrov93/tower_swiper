﻿using DG.Tweening;
using Game.Board;
using GameInput;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;
using Zenject;

namespace Game.Characters.Detail
{
    public abstract class Navigator : MonoBehaviour, INavigator
    {
        public event Action<Int2> NavigatorArrived;

        [SerializeField]
        private Int2 coords = Int2.zero;

        [Inject]
        private IBoardManager boardManager;

        [SerializeField]
        private float moveSpeed = 3f;

        protected bool moving = false;

        public Int2 Coords
        {
            get { return coords; }
            private set { coords = value; }
        }

        public ITile CurrentTile
        {
            get { return boardManager.GetTile(Coords); }
        }

        public void SetPos(Int2 newCoords)
        {
            transform.DOKill();
            moving = false;
            Coords = newCoords;
            ITile target = boardManager.GetTile(newCoords);
            if (target != null)
            {
                transform.position = target.WorldPos;
            }
        }

        public void DOMovement(Direction direction, Ease ease = Ease.InOutCubic)
        {
            ITile target = boardManager.GetTileInDirection(direction, coords);
            if (target != null && !target.IsObstructed && !moving)
            {
                Coords += Int2.DirectionToInt(direction);
                transform.DOMove(CurrentTile.WorldPos, (CurrentTile.WorldPos - transform.position).magnitude / moveSpeed)
                    .SetEase(ease).OnComplete(OnMovementFinished);
                moving = true;
            }
        }

        protected virtual void OnMovementFinished()
        {
            moving = false;
            RaiseNavigatorArrived();
        }

        private void RaiseNavigatorArrived()
        {
            if (NavigatorArrived != null)
            {
                NavigatorArrived(Coords);
            }
        }
    }
}
namespace Game.Characters
{ 
    public interface INavigator
    {
        ITile CurrentTile { get; }

        Int2 Coords { get; }

        void DOMovement(Direction direction, Ease ease = Ease.InOutCubic);
    }
}