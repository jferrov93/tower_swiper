﻿using Game.Characters;
using Game.Characters.Detail;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;
using Zenject;

namespace Game.Board.Detail
{
    public class TowerSpikes : MonoBehaviour
    {
        [SerializeField]
        private RoutedNavigator towerSpikePrefab;
        [SerializeField]
        private float spikeDelay = 3f;

        [Inject]
        private IBoardManager boardManager;
        [Inject]
        private IPlayer player;
        [Inject]
        private DiContainer container;

        private int currentFloor = 0;

        private void Start()
        {
            StartCoroutine(DelayedSpike(currentFloor));
            player.PlayerMoved += OnPlayerMovement;
        }

        private void OnPlayerMovement(Int2 playerCoords)
        {
            if (playerCoords.x > currentFloor)
            {
                currentFloor++;
                StartCoroutine(DelayedSpike(currentFloor));
            }
        }

        private IEnumerator DelayedSpike(int targetFloor)
        {
            yield return new WaitForSeconds(spikeDelay);
            RoutedNavigator spike = container.InstantiatePrefab(towerSpikePrefab).GetComponent<RoutedNavigator>();
            spike.transform.position = boardManager.GetTile(targetFloor * Int2.DirectionToInt(Direction.up)).WorldPos - Vector3.right*3f;
            spike.SetPos(new Int2(targetFloor, -1));
        }
    }
}