﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

namespace Game.Board.Detail
{
    [Serializable]
    public class Board
    {
        [SerializeField]
        private List<TileRow> board;

        public TileRow this[int i]
        {
            get { return board[i]; }
        }

        public int RowCount
        {
            get { return board.Count; }
        }

        public Board(int rows, int columns)
        {
            board = new List<TileRow>();
            for(int i = 0; i < rows; i++)
            {
                board.Add( new TileRow(columns));
            }
        }

        public void AddRow(TileRow newRow)
        {
            board.Add(newRow);
        }

        public TileBase GetTileInDirection(Direction direction, Int2 startCoord)
        {
            Int2 step = Int2.DirectionToInt(direction);
            Int2 targetCoord = startCoord + step;
            if (targetCoord.x < 0 || targetCoord.y < 0 || targetCoord.x > RowCount -1 || targetCoord.y > board[0].Length -1) { return null; }
            return board[targetCoord.x][targetCoord.y];
        }
    } 

    [Serializable]
    public class TileRow
    {
        [SerializeField]
        private TileBase[] tiles;

        public TileBase this[int i]
        {
            get { return tiles[i]; }
            set { tiles[i] = value; }
        }

        public int Length
        {
            get { return tiles.Length; }
        }

        public TileRow(int rowLength)
        {
            tiles = new TileBase[rowLength];
        }


    }
}