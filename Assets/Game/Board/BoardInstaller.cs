﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

namespace Game.Board.Detail
{
    public class BoardInstaller : MonoInstaller
    {
        [SerializeField]
        private BoardManager boardManager;

        public override void InstallBindings()
        {
            Container.Bind<IBoardManager>().FromInstance(boardManager);
            Container.Bind<BoardManager>().FromInstance(boardManager);
        }
    }
}