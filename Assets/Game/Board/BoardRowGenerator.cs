﻿using Game.Characters;
using UnityEngine;
using Util;
using Zenject;

namespace Game.Board.Detail
{
    public class BoardRowGenerator : MonoBehaviour
    {
        [SerializeField]
        private int preloadedFloors;
        [SerializeField]
        private TilesData tileData;

        [Inject]
        private BoardManager boardManager;
        [Inject]
        private IPlayer player;
        [Inject]
        private DiContainer container;

        public int currentTopFloor = 0;
        private float tileDistance;

        [Inject]
        private void Inject()
        {
            CalculateDistances();
            currentTopFloor = boardManager.board.RowCount - 1;
            player.PlayerMoved += OnPlayerMoved;
        }

        private void CalculateDistances()
        {
            tileDistance = (boardManager.board[0][1].WorldPos - boardManager.board[0][0].WorldPos).x;
        }

        private void OnPlayerMoved(Int2 playerCoords)
        {
            if (playerCoords.x >= currentTopFloor - preloadedFloors)
            {
                LoadNewFloor();
            }
        }

        private void LoadNewFloor()
        {
            TileRow newRow = new TileRow(boardManager.board[currentTopFloor].Length);
            bool elevatorAdded = false;
            for (int i = 0; i < newRow.Length; i++)
            {
                TileBase newTile = null;
                if (!elevatorAdded && i == (newRow.Length - 1))
                {
                    newTile = GenerateElevator();
                }
                else
                {
                    newTile = GenerateRandomTile();
                }
                if (newTile.GetType() == typeof(TileElevator))
                {
                    elevatorAdded = true;
                }
                newTile.transform.parent = boardManager.transform;
                newTile.transform.position = (boardManager.board[0][0].WorldPos + new Vector3( i, (currentTopFloor + 1))) * tileDistance;
                newRow[i] = newTile;

            }
            boardManager.board.AddRow(newRow);
            currentTopFloor++;
        }

        private TileBase GenerateRandomTile()
        {
            int randomIndex = Random.Range(0, tileData.Tiles.Length);
            return container.InstantiatePrefab(tileData.Tiles[randomIndex]).GetComponent<TileBase>();
        }

        private TileBase GenerateElevator()
        {
            return container.InstantiatePrefab(tileData.ElevatorTile).GetComponent<TileBase>();
        }
    }
}