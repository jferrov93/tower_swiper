﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;

namespace Game.Board.Detail
{
    public class BoardManager : MonoBehaviour, IBoardManager
    {
        public Board board;
        
        public ITile GetTileInDirection(Direction direction, Int2 startCoord)
        {
            return board.GetTileInDirection(direction, startCoord);
        }

        public ITile GetTile(Int2 coords)
        {
            try
            {
                return board[coords.x][coords.y];
            }
            catch (IndexOutOfRangeException e)
            {
                return null;
            }
        }
    }
}

namespace Game.Board
{
    public interface IBoardManager
    {
        ITile GetTileInDirection(Direction direction, Int2 startCoord);

        ITile GetTile(Int2 coords);
    }
}