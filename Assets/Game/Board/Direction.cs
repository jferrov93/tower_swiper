﻿namespace Game
{
    public enum Direction
    {
        none,
        up,
        right,
        left,
        down
    }
}