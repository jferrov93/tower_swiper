﻿using Game.Characters;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Board.Detail
{
    public class TileElevator : TileInteractableBase
    {
        public override void Interact(IPlayer player)
        {
            player.PlayerMovement.DOMovement(Direction.up, DG.Tweening.Ease.OutCubic);
        }

        public override void OnPlayerArrival(IPlayer player)
        {
        }
    }
}