﻿using Game.Characters;
using Game.Interactive.Detail;
using System;
using System.Collections;
using UnityEngine;

namespace Game.Board.Detail
{
    public class TileSpikes : TileBase
    {
        [SerializeField]
        private Spikes spikes;
        [SerializeField]
        private float spikesInterval = 1f, spikeDelay = 0.6f;
        [SerializeField]
        private SpikeType spikeType = SpikeType.timer;

        private SpriteRenderer spriteRenderer;

        private bool spikesOut = false;

        public bool SpikesOut
        {
            get { return spikesOut; }
        }

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void Start()
        {
            if(spikeType == SpikeType.timer)
                StartCoroutine(SpikesCoroutine());
            spikes.ChangedActivationStep += OnChangedActivationStep;
        }

        private void OnChangedActivationStep(int step)
        {
            switch (step)
            {
                case 0:
                    spriteRenderer.color = Color.white;
                    break;
                case 1:
                    spriteRenderer.color = Color.red;
                    break;
            }
        }

        public override void OnPlayerArrival(IPlayer player)
        {
            if (spikeType == SpikeType.delayedTrigger)
            {
                StopAllCoroutines();
                StartCoroutine(DelayedSpike());
            }
        }

        private IEnumerator DelayedSpike()
        {
            yield return new WaitForSeconds(spikeDelay);
            spikes.Activate(true);
            yield return new WaitForSeconds(spikesInterval);
            spikes.Activate(false);
        }

        private IEnumerator SpikesCoroutine()
        {
            while (spikesInterval > 0)
            {
                yield return new WaitForSeconds(spikesInterval);
                spikes.ToggleActive();
            }
        }

    }
    internal enum SpikeType
    {
        timer,
        delayedTrigger
    }
}