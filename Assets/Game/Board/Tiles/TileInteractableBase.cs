﻿using System.Collections;
using System.Collections.Generic;
using Game.Characters;
using Game.Interactive;
using UnityEngine;

namespace Game.Board.Detail
{
    public abstract class TileInteractableBase : TileBase, IIneractable
    {
        public bool Used
        {
            get; private set;
        }

        public abstract void Interact(IPlayer player);

        protected void SetUsed()
        {
            Used = true;
        }
    }
}