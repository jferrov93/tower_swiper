﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Board.Detail
{
    [CreateAssetMenu(fileName = "TilePrefabsData", menuName = "Data/TilePrefabData")]
    public class TilesData : ScriptableObject
    {
        [SerializeField]
        private TileBase[] tiles;

        public TileBase[] Tiles
        {
            get { return tiles; }
        }

        public TileBasic BasicTile
        {
            get { return GetTileOfType<TileBasic>(); }
        }

        public TileElevator ElevatorTile
        {
            get { return GetTileOfType<TileElevator>(); }
        }

        public T GetTileOfType<T>() where T : TileBase
        {
            foreach (TileBase tile in tiles)
            {
                if (tile.GetType() == typeof(T))
                {
                    return tile as T;
                }
            }
            return null;
        }
    }
}