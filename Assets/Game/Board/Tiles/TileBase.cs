﻿using Game.Characters;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Board.Detail
{
    public abstract class TileBase : MonoBehaviour, ITile
    {
        [SerializeField]
        private List<BoardElement> tileElements = new List<BoardElement>();

        public List<BoardElement> TileElements
        {
            get { return new List<BoardElement>(tileElements); }
        }

        public Vector3 WorldPos
        {
            get
            {
                return transform.position;
            }
        }

        public bool IsObstructed
        {
            get { return (tileElements.Find(t => t.ObstructingElement) != null); }
        }

        private void Awake()
        {
            tileElements = new List<BoardElement>( GetComponentsInChildren<BoardElement>() );
            foreach(BoardElement element in tileElements)
            {
                element.ElementDestroyed += OnElementDestroyed;
            }
        }

        public void AddElement(BoardElement elementToAdd)
        {
            tileElements.Add(elementToAdd);
            elementToAdd.transform.position = transform.position;
            elementToAdd.transform.parent = transform;
            elementToAdd.ElementDestroyed += OnElementDestroyed;
        }

        private void OnElementDestroyed(BoardElement element)
        {
            element.ElementDestroyed -= OnElementDestroyed;
            tileElements.Remove(element);
        }

        public abstract void OnPlayerArrival(IPlayer player);
    }
}

namespace Game.Board
{
    public interface ITile
    {
        Vector3 WorldPos { get; }
        List<BoardElement> TileElements { get; }
        bool IsObstructed { get; }
        void OnPlayerArrival(IPlayer player);
        void AddElement(BoardElement elementToAdd);
    }
}