﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Util;
using Zenject;

namespace Game.Board
{
    public class BoardElement : MonoBehaviour
    {
        public event Action<BoardElement> ElementDestroyed;

        [SerializeField]
        private Int2 coords = Int2.zero;

        [Inject]
        protected IBoardManager boardManager;

        public Int2 Coordinates
        {
            get { return coords; }
            set { SetCoordinates(value); }
        }

        public virtual bool ObstructingElement
        {
            get { return false; }
        }

        public void SetCoordinates(Int2 newCoords)
        {
            coords = newCoords;
        }

        private void OnDestroy()
        {
            if (ElementDestroyed != null)
                ElementDestroyed(this);
        }
    }
}