﻿using Game.Damage;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Board.Detail
{
    public class Wall : BoardElement, IDamageable
    {
        public void Damage()    
        {
            Destroy(gameObject);
        }

        public override bool ObstructingElement
        {
            get
            {
                return true;
            }
        }
    }
}