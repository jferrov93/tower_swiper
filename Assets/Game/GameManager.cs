﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Detail
{
    public class GameManager : MonoBehaviour, IGameManager
    {
        public void Restart()
        {
            StartCoroutine(DelayedRestart());
        }

        private IEnumerator DelayedRestart()
        {
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }
}

namespace Game
{
    public interface IGameManager
    {
        void Restart();
    }
}