﻿using Game;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Util
{
    [Serializable]
    public struct Int2
    {
        public static Int2 zero = new Int2(0, 0);
        public static Int2 up = new Int2(0, 1);
        public static Int2 down = new Int2(0, -1);
        public static Int2 right = new Int2(1, 0);
        public static Int2 left = new Int2(-1, 0);

        public int x;
        public int y;

        public Int2(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static Int2 DirectionToInt (Direction direction)
        {
            switch (direction)
            {
                case Direction.right:
                   return up;
                case Direction.up:
                    return right;
                case Direction.down:
                    return left;
                case Direction.left:
                    return down;
                default:
                    return zero;
            }
        }

        public static Int2 SquareClamp(Int2 target, Int2 minXY, Int2 maxXY)
        {
            int x = Mathf.Clamp(target.x, minXY.x, maxXY.x);
            int y = Mathf.Clamp(target.y, minXY.y, maxXY.y);
            return new Int2(x, y);
        }

        public override bool Equals(object obj)
        {
            return obj != null && obj is Int2 && ((Int2)obj).x == x && ((Int2)obj).y == y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1502939027;
            hashCode = hashCode * -1521134295 + base.GetHashCode();
            hashCode = hashCode * -1521134295 + x.GetHashCode();
            hashCode = hashCode * -1521134295 + y.GetHashCode();
            return hashCode;
        }

        public static bool operator ==(Int2 a, Int2 b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Int2 a, Int2 b)
        {
            return !a.Equals(b);
        }

        public static Int2 operator +(Int2 a, Int2 b)
        {
            return new Int2(a.x + b.x, a.y + b.y);
        }

        public static Int2 operator *(int integer, Int2 a)
        {
            return new Int2(a.x * integer, a.y * integer);
        }
    }
}